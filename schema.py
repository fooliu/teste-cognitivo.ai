from typing import Any
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, String
from sqlalchemy import ForeignKey, Boolean, Date
from sqlalchemy.orm import relationship

Base = declarative_base()  # type: Any


class Component(Base):
    __tablename__ = 'component'
    component_id = Column(String, primary_key=True, nullable=False)
    component_type_id = Column(String)
    type = Column(String)
    connection_type_id = Column(String)
    outside_shape = Column(String)
    base_type = Column(String)
    height_over_tube = Column(Float)
    bolt_pattern_long = Column(Float)
    bolt_pattern_wide = Column(Float)
    groove = Column(String)
    base_diameter = Column(Float)
    shoulder_diameter = Column(Float)
    unique_feature = Column(String)
    orientation = Column(String)
    weight = Column(Float)

    def __repr__(self):
        def _attr(x):
            val = getattr(self, x)
            return None if val is None else (x + '=' + val)
        xs = map(_attr, [
            'component_id', 'component_type_id', 'type',
            'connection_type_id', 'outside_shape', 'base_type',
            'height_over_tube', 'bolt_pattern_long', 'bolt_pattern_wide',
            'groove', 'base_diameter', 'shoulder_diameter', 'unique_feature',
            'orientation', 'weight'])
        xs = filter(lambda x: x is not None, xs)
        attrs = ' '.join(xs)
        return f'<Component {attrs}>'


class TubeAssembly(Base):
    __tablename__ = 'tube_assembly'
    tube_assembly_id = Column(String, primary_key=True, nullable=False)
    components = relationship('TubeAssemblyComponent',
                              order_by='TubeAssemblyComponent.component_seq')

    def __repr__(self):
        return (f'<TubeAssembly id={self.tube_assembly_id} '
                f'#components={len(self.components)}>')


class TubeAssemblyComponent(Base):
    __tablename__ = 'tube_assembly_component'
    tube_assembly_id = Column(
            String,
            ForeignKey('tube_assembly.tube_assembly_id'),
            primary_key=True,
            nullable=False)
    component_seq = Column(Integer, primary_key=True, nullable=False)
    component_id = Column(
            String, ForeignKey('component.component_id'), nullable=False)
    quantity = Column(Integer)
    component = relationship('Component')
    tube_assembly = relationship('TubeAssembly')

    def __repr__(self):
        return (f'<TubeAssemblyComponent seq={self.component_seq} '
                f'component={self.component_id}>')


class PriceQuote(Base):
    __tablename__ = 'price_quote'
    tube_assembly_id = Column(
            String,
            ForeignKey('tube_assembly.tube_assembly_id'),
            primary_key=True,
            nullable=False)
    supplier = Column(String, primary_key=True)
    quote_date = Column(Date, primary_key=True)
    annual_usage = Column(Integer, primary_key=True)
    min_order_quantity = Column(Integer, primary_key=True)
    bracket_pricing = Column(Boolean, primary_key=True)
    quantity = Column(Integer, primary_key=True)
    cost = Column(Float, nullable=False)
    tube_assembly = relationship('TubeAssembly')

    def __repr__(self):
        if self.bracket_pricing:
            return (f'<PriceQuote cost={self.cost} '
                    f'tube_assembly={self.tube_assembly_id} '
                    f'quote_date={self.quote_date} '
                    f'annual_usage={self.annual_usage} '
                    f'quantity={self.quantity}>')
        else:
            return (f'<PriceQuote cost={self.cost} '
                    f'tube_assembly={self.tube_assembly_id} '
                    f'quote_date={self.quote_date} '
                    f'annual_usage={self.annual_usage} '
                    f'min_order_quantity={self.min_order_quantity}>')
