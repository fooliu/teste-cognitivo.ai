import io
import click
import schema
import pandas as pd
from sanic import Sanic
from sanic.response import json, html
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, lazyload
from sanic.exceptions import SanicException
import jinja2

app = Sanic()
engine = create_engine('sqlite:///company.db')
Session = sessionmaker(bind=engine)
env = jinja2.Environment(
    loader=jinja2.FileSystemLoader('./templates'),
    autoescape=jinja2.select_autoescape(['html', 'xml'])
)


COMPONENT_COLUMNS = (
    'component_id',
    'component_type_id',
    'type',
    'connection_type_id',
    'outside_shape',
    'base_type',
    'height_over_tube',
    'bolt_pattern_long',
    'bolt_pattern_wide',
    'groove',
    'base_diameter',
    'shoulder_diameter',
    'unique_feature',
    'orientation',
    'weight'
)

TUBE_ASSEMBLY_COLUMNS = (
    'tube_assembly_id',
    'component_id_1',
    'quantity_1',
    'component_id_2',
    'quantity_2',
    'component_id_3',
    'quantity_3',
    'component_id_4',
    'quantity_4',
    'component_id_5',
    'quantity_5',
    'component_id_6',
    'quantity_6',
    'component_id_7',
    'quantity_7',
    'component_id_8',
    'quantity_8'
)

PRICE_QUOTE_COLUMNS = (
    'tube_assembly_id',
    'supplier',
    'quote_date',
    'annual_usage',
    'min_order_quantity',
    'bracket_pricing',
    'quantity',
    'cost',
)


@app.exception(SanicException)
async def handle_server_error(req, exc):
    """ Disable this in production! """
    return json({
        'status': 'error',
        'message': exc.message,
    })


async def put_tube_assembly(data):
    if data is None:
        return False
    df = pd.read_csv(io.StringIO(data.body.decode()))
    if tuple(df.columns) != TUBE_ASSEMBLY_COLUMNS:
        return False

    tubes = []
    for k, (_, row) in enumerate(df.iterrows()):
        ta = schema.TubeAssembly(tube_assembly_id=row.tube_assembly_id)
        for i in range(1, 9):
            key = f'component_id_{i}'
            if pd.isna(row[key]):
                break
            c = schema.TubeAssemblyComponent(
                component_seq=i-1,
                component_id=row[key],
                quantity=row[f'quantity_{i}'])
            ta.components.append(c)
        tubes.append(ta)

    session = Session()
    session.add_all(tubes)
    session.commit()
    return True


async def put_components(data):
    if data is None:
        return False
    df = pd.read_csv(io.StringIO(data.body.decode()))
    if tuple(df.columns) != COMPONENT_COLUMNS:
        return False

    session = Session()
    args = df.to_dict(orient='records')
    session.add_all([schema.Component(**x) for x in args])
    session.commit()
    return True


async def put_price_quotes(data):
    if data is None:
        return False
    df = pd.read_csv(io.StringIO(data.body.decode()),
                     parse_dates=['quote_date'])
    df.bracket_pricing = df.bracket_pricing == 'Yes'
    if tuple(df.columns) != PRICE_QUOTE_COLUMNS:
        return False

    session = Session()
    args = df.to_dict(orient='records')
    session.add_all([schema.PriceQuote(**x) for x in args])
    session.commit()
    return True


@app.get('/')
async def list_price_quotes(req):
    limit = int(req.args.get('limit', '10'))
    offset = int(req.args.get('offset', '0'))
    PQ = schema.PriceQuote
    session = Session()
    res = session.query(PQ) \
        .options(lazyload(PQ.tube_assembly)) \
        .order_by(PQ.quote_date.desc()) \
        .offset(offset * limit) \
        .limit(limit).all()
    tpl = env.get_template('prices.html')
    prev_page = app.url_for(
        'list_price_quotes', offset=max(0, offset-1), limit=limit)
    next_page = app.url_for(
        'list_price_quotes', offset=offset+1, limit=limit)
    return html(tpl.render(
        prices=res,
        prev_page=prev_page,
        next_page=next_page,
    ))


@app.post('/upload')
async def post_upload_data(req):
    res1 = await put_components(req.files.get('components'))
    res2 = await put_tube_assembly(req.files.get('tubes'))
    res3 = await put_price_quotes(req.files.get('prices'))
    if res1 and res2 and res3:
        tpl = env.get_template('upload.html')
        return html(tpl.render(success=True))
    else:
        tpl = env.get_template('upload.html')
        return html(tpl.render(success=False))


@app.get('/upload')
async def get_upload_data(req):
    tpl = env.get_template('upload.html')
    return html(tpl.render())


@app.get('/component/<cid>')
async def get_component(req, cid):
    CO = schema.Component
    session = Session()
    res = session.query(CO).get(cid)
    if not res:
        tpl = env.get_template('notfound.html')
        return html(tpl.render(
            my_title='Component not found',
            message=f'The component {cid} was not found!'))
    tpl = env.get_template('component.html')
    comp = {k: getattr(res, k) for k in COMPONENT_COLUMNS}
    return html(tpl.render(comp=comp))


@app.get('/tube-assembly/<tube_id>')
async def get_tube(req, tube_id):
    TA = schema.TubeAssembly
    session = Session()
    res = session.query(TA).get(tube_id)
    if not res:
        tpl = env.get_template('notfound.html')
        return html(tpl.render(
            my_title='Tube Assembly not found',
            message=f'The tube assembly {tube_id} was not found!'))
    tpl = env.get_template('tube.html')
    return html(tpl.render(tube=res))


@click.group()
def main():
    pass


@main.command()
def start():
    app.run(host='0.0.0.0', port=8000)


@main.command()
def initdb():
    click.echo('Creating databases...')
    schema.Base.metadata.create_all(engine)
    click.echo('Creating databases...done')


if __name__ == '__main__':
    main()
