FROM python:3

COPY . /var/app
WORKDIR /var/app

RUN pip install pipenv \
	&& pipenv install --system --deploy \
	&& python app.py initdb

CMD [ "python", "app.py", "start" ]
