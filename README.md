# Teste Engenheiro de Dados Cognitivo.ai

## Arquitetura & Modelagem

![diagram](images/diagram.png)

Os dados entregues descrevem componentes (entidade *Component*) que são
usados para fabricar *tubos* (entidade *TubeAssembly*). Um tubo é
composto por diversos componentes, onde cada componente pode ser usado
mais de uma vez—essa multiplicidade é representada pelo atributo
*quantity* da entidade *TubeAssembly Component*.

A entidade *PriceQuote* representa orçamentos de tubos. Os orçamentos
dependem de uma série de fatores, como tipo do tubo, data, fornecedor, e
estratégia de precificação.

Existem duas estratégias de precificação: *bracket pricing* e
*non-bracket pricing*. Na estratégia *bracket*, o preço depende da
quantidade de tubos que será adquirida, ao passo que na estratégia
*non-bracket* o preço só vale para uma compra mínima de tubos. Aqui vale
ressaltar uma nota:

**:warning: Tentei separar os orçamentos em duas tabelas, uma para cada
estratégia de precificação. Entretanto, ao analisar os dados, percebi
que a quantidade não especificava unicamente um orçamento na
estratégia *bracket*. Veja o exemplo abaixo:**

|tube\_assembly\_id|supplier|quote\_date|annual\_usage|min\_order\_quantity|bracket\_pricing|quantity|cost|
|------------------|--------|-----------|-------------|--------------------|----------------|--------|----|
|TA-08147|S-0066|2011-08-01|1|5|Yes|1|13.476294|
|TA-08147|S-0066|2011-08-01|1|1|Yes|1|23.021539|
|TA-08147|S-0066|2011-08-01|15|10|Yes|1|23.021539|

Neste exemplo, podemos ver que as colunas *tube_assembly_id*,
*supplier*, *quote_date*, e *quantity* **não** identificam unicamente os
orçamentos, logo esses valores não podem ser chave privada do orçamento.
Por esse motivo, resolvi tornar todas as colunas (exceto a coluna
*cost*) em chave primária da tabela *PriceQuote*.

Outro problema que encontrei nos dados são tubos sem nenhum componente e
tubos com componentes inexistentes.

## Implementação

Para modelar a base de dados, utilizei a biblioteca
[SQLAlchemy](https://www.sqlalchemy.org/) para o Python e criei uma API
com a biblioteca [Sanic](https://github.com/huge-success/sanic).

A API expõe três rotas para enviar os arquivos CSV. São elas:

 * `PUT /components` que recebe uma requisição *form* com o arquivo que
   descreve os componentes,
 * `PUT /tube-assembly` que recebe uma requisição *form* com o arquivo
   que descreve os tubos,
 * e `PUT /price-quotes` que recebe uma requisição *form* com o arquivo
   que descreve os orçamentos.

Além disso, a API também expõe rotas para listar orçamentos e descrever
tubos e componentes:

 * `GET /componente/<ID>`
 * `GET /tube-assembly/<ID>`
 * `GET /price-quotes`

## Executar

Para executar, certifique que o servico do [Docker](https://docker.com)
esteja funcionando e prossiga executando `make docker && make start`.

Visualize os sistema apontando o seu *browser* para
[localhost:8000](http://localhost:8000).

### Exemplo de Uso

#### Página Inicial
![1](images/01.png)

<!-- #### Upload de Arquivos
![2](images/02.png) -->

#### Upload de Arquivos
![3](images/03.png)

#### Upload de Arquivos Sucedido
![4](images/04.png)

#### Lista de Cotações
![5](images/05.png)

#### Tube Assembly
![6](images/06.png)

#### Component
![7](images/07.png)

## Limpar

Para limpar as imagens Docker criadas, execute `make docker-clean`.
Nenhum outro arquivo é gerado fora do diretório do repositório, então
basta remove-lo para limpar tudo.

<!--
vim:tw=72:sw=2:et:sta
-->
