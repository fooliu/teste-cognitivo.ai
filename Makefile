local: company.db
	pipenv run python app.py start

company.db:
	pipenv run python app.py initdb

start:
	docker run --rm -p 8000:8000 test-ian-cognitivo-ai

docker:
	docker build -t test-ian-cognitivo-ai .

docker-clean:
	docker rmi test-ian-cognitivo-ai
